(in-package :nyxt-user)


(define-configuration browser
                      ((session-restore-prompt :always-restore)))

;; use vi mode
(define-configuration buffer
                      ((default-modes (append '(nyxt::vi-normal-mode) %slot-default%))))

(define-configuration prompt-buffer
                      ((default-modes (append '(nyxt::vi-insert-mode) %slot-default%))))

(define-configuration window
                      ((message-buffer-style
                         (str:concat
                           %slot-default%
                           (cl-css:css '((body :background-color "black"
                                               :color "lightgray")))))))

(define-configuration web-buffer
                      ((default-modes (append
                                        '(auto-mode
                                           blocker-mode
                                           force-https-mode
                                           ;; noimage-mode
                                           ;; noscript-mode
                                           reduce-tracking-mode)
                                        %slot-default%))))

(define-configuration #+nyxt-2 buffer #+nyxt-3 context-buffer
  ((search-engines (list
                    (make-instance 'search-engine
                                   :shortcut ":osm"
                                   :search-url "https://www.openstreetmap.org/search?query=~a"
                                   :fallback-url (quri:uri "https://www.openstreetmap.org/"))
                    (make-instance 'search-engine
                                   :shortcut ":br"
                                   :search-url "https://search.brave.com/search?q=~a&source=desktop")
                    (make-instance 'search-engine
                                   :shortcut ":g"
                                   :search-url "https://www.google.com/search?q=~a")
                    (make-instance 'search-engine
                                   :shortcut ":sp"
                                   :search-url "https://www.startpage.com/do/search?q=~a&segment=startpage.brave")
                    (make-instance 'search-engine
                                   :shortcut ":aw"
                                   :search-url "https://wiki.archlinux.org/index.php?title=Special:Search&search=~a")
                    (make-instance 'search-engine
                                   :shortcut ":yi"
                                   :search-url "https://yandex.ru/images/search?text=~a"
                                   :fallback-url (quri:uri "https://yandex.ru/images/"))
                    (make-instance 'search-engine
                                   :shortcut ":y"
                                   :search-url "https://yandex.com/search/?text=~a"
                                   :fallback-url (quri:uri "https://yandex.com/search/"))))))
