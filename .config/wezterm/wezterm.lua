local wezterm = require 'wezterm';
local act = wezterm.action

return {
    -- Colors
    color_scheme = "Dracula",
    window_background_opacity = 0.93,

    -- Cursor
    cursor_blink_rate = 0, -- do not blink

    -- Better defaults
    hide_tab_bar_if_only_one_tab = true,
    adjust_window_size_when_changing_font_size = false,
    scrollback_lines = 10000,
    enable_scroll_bar = false,
    exit_behavior="Close",

    -- Keys
    use_dead_keys = false,
    disable_default_key_bindings = true,
    keys = {
        -- Clipboard
        {key="c", mods="CTRL|SHIFT", action=act{CopyTo="Clipboard"}},
        {key="v", mods="CTRL|SHIFT", action=act{PasteFrom="Clipboard"}},
        {key="c", mods="ALT", action=act{CopyTo="Clipboard"}},
        {key="v", mods="ALT", action=act{PasteFrom="Clipboard"}},

        -- Increase size
        {key="=", mods="CTRL", action="IncreaseFontSize"},
        {key="-", mods="CTRL", action="DecreaseFontSize"},

        -- Tabs
        {key="t", mods="CTRL|SHIFT", action=act{SpawnTab="CurrentPaneDomain"}},
        {key="w", mods="CTRL|SHIFT", action=act{CloseCurrentTab={confirm=false}}},
        {key="LeftArrow",  mods="CTRL|ALT", action=act{ActivateTabRelative=-1}},
        {key="RightArrow", mods="CTRL|ALT", action=act{ActivateTabRelative=1}},

        -- Modes (Search / Copy)
        {key="f", mods="CTRL|SHIFT", action=act{Search={CaseInSensitiveString=""}}},
        {key=" ", mods="CTRL|SHIFT", action="ActivateCopyMode"},

        -- Clears the scrollback and viewport, and then sends CTRL-L to ask the
        -- shell to redraw its prompt
        {key="l", mods="CTRL",
            action = act.Multiple {
                act.ClearScrollback 'ScrollbackAndViewport',
                act.SendKey { key = 'L', mods = 'CTRL' },
            },
        },
    },

    mouse_bindings = {
        -- Paste using mouse
        {
            event={Up={streak=1, button="Right"}},
            mods = 'NONE',
            action = act{PasteFrom="Clipboard"}
        },

        -- Scrolling up while holding CTRL increases the font size
        {
            event = { Down = { streak = 1, button = { WheelUp = 1 } } },
            mods = 'CTRL',
            action = act.IncreaseFontSize,
        },

        -- Scrolling down while holding CTRL decreases the font size
        {
            event = { Down = { streak = 1, button = { WheelDown = 1 } } },
            mods = 'CTRL',
            action = act.DecreaseFontSize,
        },

        -- Change the default click behavior so that it copy selected
        -- text and doesn't open hyperlinks
        {
            event={Up={streak=1, button="Left"}},
            mods="NONE",
            action=act{CopyTo="Clipboard"},
        },

        -- do the above for double clicks
        {
            event={Up={streak=2, button="Left"}},
            mods="NONE",
            action=act{CopyTo="Clipboard"},
        },

        -- and make CTRL-Click open hyperlinks
        {
            event={Up={streak=1, button="Left"}},
            mods="CTRL",
            action="OpenLinkAtMouseCursor",
        },
        -- Disable the 'Down' event of CTRL-Click to avoid weird program behaviors
        {
            event={Down={streak=1, button="Left"}},
            mods="CTRL",
            action="Nop",
        },
    },
}
