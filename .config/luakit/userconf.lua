local modes = require "modes"
local settings = require "settings"
local select = require "select"
local follow = require "follow"

local search_engines = settings.window.search_engines

local engines = {
    default = "https://search.brave.com/search?q=%s",
    aw = "https://wiki.archlinux.org/index.php?search=%s",
    g = "https://google.com/search?q=%s",
    d = "https://duckduckgo.com/?q=%s",
    wiki = "https://en.wikipedia.org/wiki/Special:Search?search=%s",
}

for name, url in pairs(engines) do
    search_engines[name] = url
end

modes.add_binds("normal", {
    {"L", "Go to next tab", function(w) w:next_tab() end },
    {"H", "Go to prev tab", function(w) w:prev_tab() end },
    {"K", "Go back in browser history", function(w, m) w:back(m.count) end },
    {"J", "Go forward in browser history", function(w, m) w:forward(m.count) end },
})

select.label_maker = function ()
    local chars = charset("asdfqwerzxcv")
    return trim(sort(reverse(chars)))
end

follow.pattern_maker = follow.pattern_styles.match_label

settings.webview.cursive_font_family = "mono"
settings.webview.default_font_family = "mono"
settings.webview.pictograph_font_family = "mono"
settings.webview.fantasy_font_family = "mono"

settings.session.always_save = true
settings.webview.enable_media_stream = true
settings.webview.enable_mediasource = true
