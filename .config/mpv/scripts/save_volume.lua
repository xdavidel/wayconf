local utils = require "mp.utils"

local homedir = os.getenv('HOME')
local configpath = string.format("%s/.cache/mpv", homedir)
local volumepath = string.format("%s/lastvolume", configpath)

local save_volume = function()
	local file = io.open(volumepath, "w")
	local currentvolume = mp.get_property("volume")
	file:write(currentvolume)
	file:close()
end

local load_volume = function()
	local file, err = io.open(volumepath, "r")
    if err then
        os.execute("mkdir -p " .. configpath)
        return
    end

	local lastvolume = file:read()
	file:close()
	if lastvolume then
		mp.set_property("volume", lastvolume)
	end
end

-- load volume on start
load_volume()

-- save volume on exit
mp.register_event("shutdown", save_volume)
