# Arch Installation Notes

## Check for EFI Support

```sh
ls /sys/firmware/efi/efivars
```

## Partition Drives

```sh
cfdisk /dev/<DEVICE>
```

## Format Partitions

```sh
mkfs.fat -F32 -n <label> /dev/<boot-part>
```

## Drive Encryption

### Encrypt the root partition

```sh
cryptsetup luksFormat /dev/<root-part>
```

### Open the partition (decrypt)

```sh
cryptsetup open /dev/<root-part> <label>
```

### Make a file system on the root partition

```sh
mkfs.btrfs /dev/mapper/<label>
```

```sh
mkfs.ext4 -L <label> /dev/mapper/<label>
```

### Mount root partition

```sh
mount /dev/mapper/<label> /mnt
```

## Mount boot and root

```sh
mkdir /mnt/boot
mount /dev/<boot-part> /mnt/boot
```

## Update `pacman` mirrors

It is possible to sort out the mirror list `/etc/pacman.d/mirrorlist`
in an order that closer server are at the top.


The official way to update the mirrors (and probably the best) is
by using the generator:

```sh
curl -L "https://archlinux.org/mirrorlist/?country=IL&protocol=https&ip_version=4" | \
    sed 's/^#//g' | tee /etc/pacman.d/mirrorlist
```

Another option is to use the `python` script `reflector`:

```sh
pacman -S reflector
```

Sort the five most recently synchronized mirrors by download speed and overwrite the local mirrorlist:
```sh
reflector --latest 5 --sort rate --save /etc/pacman.d/mirrorlist
```

## Install the base Arch Linux

```sh
pacstrap -i /mnt base base-devel linux linux-firmware networkmanager neovim cryptsetup lvm2 grub efibootmgr os-prober
```

## Generate boot table

```sh
genfstab -U /mnt > /mnt/etc/fstab
```

## Login to the fresh installed Arch Linux

```sh
arch-chroot /mnt
```

## Set time

```sh
ln -s /usr/share/zoneinfo/<ZONE> /etc/localtime
hwclock --systohc
```

## Set Locale

`/etc/locale.conf`
```conf
export LANG="en_US.UTF-8"
export LC_COLLATE="C"
```

Uncomment the desired language in `/etc/locale.gen`

```sh
locale-gen
```

## Set Hostname

```sh
echo "Hostname" > /etc/hostname
```

`/etc/hosts`
```conf
127.0.0.1   localhost
::1         localhost

127.0.1.1   <hostname>.localdomain.<hostname>
```

## Enable Networking

```sh
systemctl enable NetworkManager.service
```

## Set Root Password

```sh
passwd
```

## Add the first user

```sh
useradd -G wheel -m user
```

## Decrypt drives on boot

Add to the HOOKS in `/etc/mkinitcpio.conf`:
```sh
encrypt lvm2
```


### Init Cpio

Regenerate the `mkinitcpio`

```sh
mkinitcpio -p <linux-kernel>
```

Check for encrypt and lvm messages...

If generating custom image

```sh
mkinitcpio --config /etc/mkinitcpio-custom.conf --generate /boot/initramfs-custom.img --kernel 6.2.11-zen1-1-zen
```

## Add boot partition UUIDs to grub

```sh
lsblk -f >> /mnt/etc/default/grub
```

/etc/default/grub
```conf
GRUB_CMDLINE_LINUX_DEFAULT="cryptdevice=UUID=<encrypted-root>:<label> root=UUID=<decrypted-root>"
```

## Bootloader

Installing GRUB

- EFI version

```sh
grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB
```

Generate GRUB configuration

```sh
grub-mkconfig -o /boot/grub/grub.cfg
```

- Legacy version

```sh
grub-install /dev/<boot-part>
```

## Misc

### sysctl

#### Increase the max memory mapping

Some programs might ask for ridiculous memory maps.
It that case they could crash with "Out of Memory" error.

To increase the limit, but still reserve some margin for
potential coredumps we can use `MAX_INT -5`:

Temporary

```sh
sysctl -w vm.max_map_count=2147483642
```

Permanently:

Add this line to:
`/etc/sysctl.d/99_override.conf`
```conf
vm.max_map_count=2147483642
```

### Systemd

#### To change closing the lid handler:

`/etc/systemd/logind.conf`
```conf
HandleLidSwitchExternalPower=ignore
```

#### Autologin

Create a drop-in file:

```sh
systemctl edit getty@tty1.service --drop-in=autologin
```

Replace the following command with the required username

`/etc/systemd/system/getty@tty1.service.d/autologin.conf`
```conf
[Service]
ExecStart=
ExecStart=-/sbin/agetty -o '-p -f -- \\u' --noclear --autologin <username> %I $TERM
```

### Network Manager

#### Disable Domain DNS

Add to the file:

`/etc/NetworkManager/NetworkManager.conf`
```conf
[main]
dns=none
rc-manager=unmanaged
```
#### MAC spoofing

Add to the file:

`/etc/NetworkManager/conf.d/wifi_rand_mac.conf`
```conf
[device-mac-randomization]
# "yes" is already the default for scanning
wifi.scan-rand-mac-address=yes

[connection-mac-randomization]
# randomize MAC for every ethernet connection
ethernet.cloned-mac-address=random
# Generate a random MAC for each WiFi and continue to use it
wifi.cloned-mac-address=stable
```

### QEMU (KVM)

To spawn kernel native virtual machines is is best to use
`qemu` and `virt-manager`.

#### Requirements

Install the following packages:

```sh
pacman -S qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat iptables-nft qemu-guest-agent
```

#### Configuration

Edit `/etc/libvirt/libvirtd.conf`

Uncomment the lines:
```conf
unix_sock_group = "libvirt
unix_sock_ro_perms = "0777"
unix_sock_rw_perms = "0770"
```

#### Add user group

```sh
usermod -aG libvirt user
```

#### Enable network

```sh
virsh net-start default
```

#### Foreign architecture emulation

This method helps to directly open binaries with
different architectures automatically.
Can be really useful for `chroot` into ARM based drive for example.

Required packages:

```sh
qemu-user-static-binfmt
```

Than enable the service in `systemd`:

```sh
sudo systemctl restart systemd-binfmt
```

#### Shared Storage

```sh
mount -t 9p -o trans=virtio /sharepoint Shared/
```
