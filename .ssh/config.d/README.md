# Additional SSH configurations

Add configuration files to this directory and they will be auto imported by the
general configuration in the upper directory.

This way I can add new configurations for other machines without the need to
commit or track them.
