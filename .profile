#!/bin/sh
# Profile file. Runs on login.

# Adds `~/.scripts` and all subdirectories to $PATH
export PATH="$PATH:$HOME/.local/bin:$(find "$HOME/.local/scripts" -type d | paste -sd ':' -)"
export EDITOR="nvim"
export FILE="vifm"

# No place like home
# ================================================================
export LESSHISTFILE="-"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_DATA_DIRS="/usr/local/share:/usr/share"

export ANDROID_SDK_HOME="$XDG_CONFIG_HOME/android"
export DOCKER_CONFIG="$XDG_CONFIG_HOME/docker"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc-2.0"
export INPUTRC="$XDG_CONFIG_HOME/inputrc"
export NPM_CONFIG_USERCONFIG="$XDG_CONFIG_HOME/npm/npmrc"
export PYTHONSTARTUP="$XDG_CONFIG_HOME/python/pythonrc"
export WEECHAT_HOME="$XDG_CONFIG_HOME/weechat"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export XINITRC="$XDG_CONFIG_HOME/x11/xinitrc"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export SCREENRC="$XDG_CONFIG_HOME/screen/screenrc"

export CARGO_HOME="$XDG_DATA_HOME/cargo"
export GOPATH="$XDG_DATA_HOME/go"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"
export SQLITE_HISTORY="$XDG_DATA_HOME/sqlite_history"

export EMACSNATIVELOADPATH="$XDG_CACHE_HOME/emacs"
export HISTFILE="$XDG_CACHE_HOME/shell_history"
export PYLINTHOME="$XDG_CACHE_HOME/pylint"
export GOMODCACHE="$XDG_CACHE_HOME/go/mod"
# ================================================================

# MPD
export MPD_HOST="127.0.0.1"
export MPD_PORT="6600"

export LOCATION="Jerusalem"

# Dotfiles
export DOTCONFDIR="${HOME}/.local/dotfiles"

# less/man colors
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"
export LESSOPEN="| /usr/bin/highlight -O ansi %s 2>/dev/null"

# Start ssh-agent if not aleady running
if [ -n "$XDG_RUNTIME_DIR" ]; then
    ! pidof ssh-agent > /dev/null && \
        ssh-agent > "$XDG_RUNTIME_DIR/ssh-agent.env" 2>/dev/null

    # Source cached ssh output
    [ -z "$SSH_AUTH_SOCK" ] && \
        source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null 2>&1
fi

# Start graphical server if not already running.
[ "$(tty)" = "/dev/tty1" ] && {
    startway
}
